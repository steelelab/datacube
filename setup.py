from setuptools import setup

setup(name='datacube',
      version='0.1',
      description='A library for loading and exploring data',
      url='https://gitlab.tudelft.nl/steelelab/datacube',
      author='Gary Steele',
      author_email='g.a.steele@tudelft.nl',
      license='MIT',
      packages=['datacube'],
      zip_safe=False)
