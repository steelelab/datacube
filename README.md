<!-- #region -->
# The Datacube library v0.1

## What is it? 

A library I have started to write to replace (at least some) of the functionality of our well-loved but now aging spyview data visualisation tool:

https://nsweb.tn.tudelft.nl/~gsteele/spyview/

The datacube library does this by building interactive widgets and plots directly inline in your Jupyter notebook.

The widgets, interaction, and GUI event loop are provided by the ipywidgets library:

https://ipywidgets.readthedocs.io/en/stable/

The "live" plotting is handled by the (javascript-based?) matplotlib `notebook` driver, which makes it relatively fast and responsive. 

It is designed to parse metadata about axis names, ranges, and label automatically from your data files (as long as that metadata is supplied somehow, for now spyview meta.txt files are supported), and the produce line-cut plots and colorplot images of multidimensional data with one command. 

It also has support for "tweaking" the colors in your colorscale plot, and also interactively exploring linecuts of your data.

## How to use it

You should first load the library. You can then create a datacube object, load data into it, and use the functions to generate interactive plots. Hopefully we will evolve a nice set of documentation, but you can see some example uses cases in the testing and debugging example notebook:

https://gitlab.tudelft.nl/steelelab/datacube/blob/master/datacube_examples.ipynb

The exmaple notebook will generate for you a `.dat` file with metadata. If you are running this on the steelelab jupyterhub, you can also directly run the cells for the example with a "real" data file from our measurement folders. 

## Spyview-style image processing

For the image data, `datacube` makes use of the `mtx` object class from Mark Jenkins' `stlabutils` library:

https://steelelab-delft.github.io/stlabutils/utils/stlabdict.html#stlabutils.utils.stlabdict.stlabmtx.mtx

In this class, Mark has actually implemented a huge set of the standard data processing operations we use in spyview! 

He has implemented this as well in a flexible way (a bit like the spyview "Image Processing" window) with a copy of the original data and a running list of the image processing operations that are performed to produce the processed data. 

Using the mtx object inside the datacube object, you have access to all these functions!

For example, a simple one and common workflow might be:

```
dc = Datacube()
dc.load_dat_meta("test.dat")
dc.create_mtx_cut(cut_axis = 0, col=1)
dc.mtx.sub_lbl()
dc.interactive_image()
```

Right now, you will have to read Mark's documenation page above to get the full list and descriptions of image processing routines, but soon I hope to implement a GUI interface where you can browse through all the options, click on them to get the documentation, set parameters, add them to the queue, etc. 


## To Do

This is only a very start. There are lots of things we can do from here! I've put some of my thoughts in the gitlab issue list as feature requests:

https://gitlab.tudelft.nl/steelelab/datacube/issues

Do you have a idea? Create an issue and tag it as a feature request!

## Bugs

There will always be bugs!

Did you find one? Please create an issue:

https://gitlab.tudelft.nl/steelelab/datacube/issues

In your issue, describe what the problem is and include a (preferrably formatted) minimal code block required to reproduce the bug.
<!-- #endregion -->
