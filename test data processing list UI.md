---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.4.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
from datacube import Datacube
import ipywidgets as widgets
from inspect import signature
```

```python
dc = Datacube()
dc.load_dat_meta("test.dat")
dc.create_mtx_cut()

methods_all = [method_name for method_name in dir(dc.mtx) 
           if callable(getattr(dc.mtx, method_name))]
to_exclude = ["__", "process", "step", 'reset', 'save', 'savemtx', 'loadmtx', 'getextents']
methods = [m for m in methods_all if not any(e in m for e in to_exclude)]

ops = widgets.Select(options=methods)
ops.layout.height = "300px"
ops.layout.width = "200px"

add = widgets.Button(description="Add =>")
empty = widgets.HTML(value="&nbsp")
remove = widgets.Button(description="Remove")
clear = widgets.Button(description="Clear")
up = widgets.Button(description="Move Up " + u"\u2191")
down = widgets.Button(description="Move Down " + u"\u2193")
controls = widgets.VBox([empty,add,remove,clear,empty,up,down])

queue = widgets.Select()
queue.layout.height = "300px"
queue.layout.width = "200px"

parameter_list = []
parameter_list.append(widgets.HTML(value="Parameters:"))
parameters = widgets.VBox(parameter_list)

cols_list = []
cols_list.append(widgets.VBox([widgets.HTML(value="Opearations:"),ops]))
cols_list.append(controls)
cols_list.append(widgets.VBox([widgets.HTML(value="Process queue:"),queue]))
cols_list.append(parameters)
cols = widgets.HBox(cols_list)

help_display = widgets.Textarea()
help_display.layout.height ='300px'
help_display.layout.width ='800px'

def update_help(w):
    help_display.value = eval(f"dc.mtx.{w.new}.__doc__")
ops.observe(update_help, names='value')

def update_process_queue(w):
    options = [s.split(" ")[0] for s in dc.mtx.processlist]
    queue.options = options

def clear_parameters(w):
    parameters.children = (parameters.children[0],)

# We will not show parameters when adding new ops
# def update_parameters_new_ops(w):
#     args = signature(eval(f'dc.mtx.{ops.value}')).parameters
#     parameters.children = (parameters.children[0],)
#     for p in args.keys():
#         if isinstance(args[p].default, bool):
#             w = widgets.Checkbox()
#             w.value = args[p].default
#         else:
#             w = widgets.Text()
#             w.value = str(args[p].default)
#         w.description = p
#         w.layout.width = "200px"
#         parameters.children += (w,)

def update_parameters(w):
    print(w.new)
    if queue.value == None:
        return
    debug_state("in update parameters")
    args = signature(eval(f'dc.mtx.{queue.value}')).parameters
    proc_string = dc.mtx.processlist[queue.index]
    if len(proc_string.split(" ") == 0):
        return
    proc_parameters = proc_string.split(" ")[1].split(",")
    parameters.children = (parameters.children[0],)
    i=0
    for p in args.keys():
        w = widgets.Text()
        w.value = proc_parameters[i]
        i+=1
#     for p in args.keys():
#         if isinstance(args[p].default, bool):
#             w = widgets.Checkbox()
#             w.value = args[p].default
#         else:
#             w = widgets.Text()
#             w.value = str(args[p].default)
#         w.description = p
#         w.layout.width = "200px"
#         parameters.children += (w,)
    
def debug_state(msg):
    print("\n"+msg)
    to_check = ['queue.index', 'queue.value', 'dc.mtx.processlist']
    for v in to_check:
        print(f"{v} is {eval(v)}")
    
def add_op(w):
    eval(f'dc.mtx.{ops.value}()')
    debug_state("in add op")
    update_queue([])
    queue.value = ops.value

def update_queue(w):
    debug_state("in update queue")
    queue.options = [s.split(" ")[0] for s in dc.mtx.processlist]        
    debug_state("after updating queue")

def remove_queue_item(w):
    del dc.mtx.processlist[queue.index]
    update_queue([])
    
def clear_queue(w):
    dc.mtx.processlist = []
    update_queue([])

clear.on_click(clear_queue)
remove.on_click(remove_queue_item)
add.on_click(add_op)
#ops.observe(update_parameters_new_ops)
ops.observe(clear_parameters)
update_queue([])
queue.observe(update_parameters)

widgets.VBox([cols, widgets.VBox([widgets.HTML(value="Help:"),help_display])])
```

```python
dc.mtx.crop()
```

```python
dc.mtx.processlist
```

```python
dc.mtx.applyprocesslist(dc.mtx.processlist)
```

# Figure out how to find the number of arguments: 

https://stackoverflow.com/questions/847936/how-can-i-find-the-number-of-arguments-of-a-python-function

```python
from inspect import signature
```

```python
sig = signature(dc.mtx.logx)
str(sig)
```

```python
args = sig.parameters
args.keys()
```

```python

```

```python
for p in args.keys():
    print(args[p].default)
```

Python is cool man!


# Add widgets to VBox dynamically

```python
box = widgets.VBox()
box.height = "600px"
box
```

```python
box.children = ()
for p in args.keys():
    w = widgets.FloatText()
    w.description = p
    w.value = args[p].default
    w.layout.width = "200px"
    box.children += (w,)
```

# Finding a list of all possible image processing routines

```python
# It would be better if the mtx class had an internal list of which ones were processing filters...
# but this hack works (for now, and probably for a while). 

methods_all = [method_name for method_name in dir(dc.mtx) 
           if callable(getattr(dc.mtx, method_name))]

to_exclude = ["__", "process", "step", 'reset', 'save', 'savemtx', 'loadmtx', 'getextents']

# I'm becomgin a real python nerd.
methods = [m for m in methods_all if not any(e in m for e in to_exclude)]
methods
```

```python
eval(f"help(dc.mtx.{methods[0]})")
```
