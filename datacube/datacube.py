# +
import numpy as np
import matplotlib.pyplot as plt
import ipywidgets as widgets
import matplotlib.colors as colors
import pandas as pd
import stlabutils

class Datacube:
    '''A class to handle loading, plotting, and interacting with multidimensional datasets.
    
    Usages: 
        
        datacube = Datacube()
        datacube.load_dat_meta("mydatafile.dat")
    
    Currently it is coded to work with .dat files configured in columns. It will look for an associated 
    meta.txt file in the same folder to try to read the metadata about the axis data (ranges, number of
    points). 
    
    (Todo) It will look for column names in a CSV format in a comment line at the top of the file. 
    
    (Todo) If it cannot find a meta.txt file, it will assume the the dataset is two dimensional and guess
    the sweep size based on the number of points in the first "datablock", assuming the the sweeps in
    the file are separated by blank lines. 
    
    The class constructor takes a single argument, the name of the .dat file. The code currently assumes
    that the file contains the literal extension ".dat"
    '''
    
    def __init__(self):
        class Axis:
            size = 1
            start = 0
            stop = 1
            name = "None"
        self.axis = [Axis(),Axis(),Axis()]         
        self.column_name = dict()
        # Hopefully, file contains at least one column
        self.default_column = 0
        self.mtx = []
        self.cut_axis = -1
        self.cut_index = -1
        self.gamma = 1
    
    # 
    # Data loading routines 
    # 
    
    def load_dat_meta(self,datfile, delimiter=','):
        # First read the metadata from the file
        metafile = datfile[:-3] + "meta.txt"
        metadata = []
        try: 
            with open(metafile) as f:
                metadata = f.read()
        except:
            print(f"Error reading metadata file:\n\n{metafile}\n")
            print(f"Trying to load data using load_dat_without_meta())\n")
            self.load_dat_without_meta(datfile)
            return
        metadata = metadata.splitlines()
        # Strip empty lines and comment lines
        metadata = [line for line in metadata if line and not line.strip().startswith("#")]

        for i in range(3):
            self.axis[i].size = int(metadata[0+i*4])
            self.axis[i].start = float(metadata[1+i*4])
            self.axis[i].stop = float(metadata[2+i*4])
            self.axis[i].name = metadata[3+i*4]
  
        # Read in the column name data. It starts after 16 lines in principle 
        # (may need to adjust this code in case people use comments in the meta.txt files...)
        # The file contains first a line with the column number (starting from 1)
        # And then a line with the name of the dataset for this column
        
        for i in range(12,len(metadata),2):
            self.column_name[int(metadata[i])-1] = metadata[i+1]
            
        self.data = np.loadtxt(datfile,delimiter=delimiter)
        
        # Check if there are the same number of data points as specified in the meta file.  
        # If not, then most likely it is an incomplete file  and we have to do some guesswork 
        # to figure out what we should do with it. 
        
        npts_meta = self.axis[0].size *  self.axis[1].size * self.axis[2].size
        npts_data = self.data.shape[0]
        if npts_data < npts_meta:
            print(f"Points in file {self.data.shape[0]} "+ 
                  f"do not match specification in meta file {npts_meta}")
            print("Suspect measurement terminated early, will try to fix")
            print("Initial axis sizes: ", end="")
            for i in range(3):
                print(self.axis[i].size, end=" ")
            print()
            if self.axis[2].size == 1 and self.axis[1].size == 1:
                # A simple case: a single sweep that did not finish
                # We can still salvage all the points
                print("Adjusting axis 0 endpoint from %e" % self.axis[0].stop, end = " ")
                self.axis[0].stop = self.axis[0].start + npts_data * \
                            (self.axis[0].stop - self.axis[0].start) / self.axis[0].size
                print("to  %e" % self.axis[0].stop)
                self.axis[0].size = self.data.shape[0]
            elif self.axis[2].size == 1:
                # A 2D sweep that did not finish
                # We will drop the last incomplete sweep
                new_ax1_pts = npts_data // self.axis[0].size
                self.axis[1].stop = self.axis[1].start + new_ax1_pts * \
                            (self.axis[1].stop - self.axis[1].start) / self.axis[1].size
                self.axis[1].size = new_ax1_pts
                new_data_size = self.axis[0].size *  self.axis[1].size * self.axis[2].size
                self.data = self.data[0:new_data_size,:]
            else:
                print("Gary has not yet coded support for incomplete 3D datasets...")
                raise ValueError
            print("Adjusted axis sizes: ", end="")
            for i in range(3):
                print(self.axis[i].size, end=" ")
            print()
            
        self.data = self.data.reshape(self.axis[0].size, 
                                      self.axis[1].size, 
                                      self.axis[2].size, 
                                      self.data.shape[1], order='F')
        
    def load_dat_without_meta(self, dat_file, sweep_column = 0, loop_column = 1):
        '''
        Load data from a .dat file that has no accompanying (or correct) meta.txt file
        
        Your data must be organised in "data blocks": sweeps separated by blank lines. 
        
        Because there is no metadata file, we will have to assume that one of the 
        columns represents the sweep values and one column represents the loop values. 
        These can be specified by the sweep_column and loop_column variables. Note that 
        the datacube library is designed under the assumption that the data in the files
        is uniformly sampled on a square grid, and will extrapolated the axis values
        based on this assumption using then endpoints of the sweep / loop columns.
        
        If you do not have a column for the loop (or sweep), you will have to specify 
        the sweep and loop variable names and start/stop values manually afterwards:
        
        dc.axis[0].start = 4
        dc.axis[0].stop = 10
        dc.axis[0].name = "Magnet current (mA)"
        
        This routine supports only 2-dimenstional parameter sweeps spaces. 

        '''
        # We will make use of Mark's code
        df_array = stlabutils.readdata.readdat(dat_file)
        self.axis[0].size = len(df_array[0])
        self.axis[1].size = len(df_array)
        self.axis[2].size = 1
        i=0
        for c in df_array[0].columns:
            self.column_name[i] = c
            i += 1
        self.data = np.zeros([self.axis[0].size,
                              self.axis[1].size,
                              self.axis[2].size,
                              len(self.column_name)])
        for i in range(len(df_array)):
            self.data[:,i,0,:] = df_array[i].values

        # Now try to fish out the extents. Usually the sweep is col 0, and the loop
        # is col 1. But if the user has done something funky, they will have to tell
        # us which is which.
        self.axis[0].start = self.data[0,0,0,sweep_column]
        self.axis[0].stop = self.data[-1,-1,0,sweep_column]
        self.axis[0].name = self.column_name[sweep_column]
        
        self.axis[1].start = self.data[0,0,0,loop_column]
        self.axis[1].stop = self.data[-1,-1,0,loop_column]
        self.axis[1].name = self.column_name[loop_column]
                
    def load_from_array(self, data):
        '''
        Load the data from a 4 (or 3 or 2) dimensional numpy array.
        
        The first 3 dimentions are the "depedent" dimensions of the array. (In a measurement, 
        this would be a nested loop sweeping parameter 1, parameter 2, and parameter 3.)
        
        The last dimension of the array allows one to pack multiple results into the same datacube.
        (In measurements, this would correspond to the different columns of the file, representing
        the different measurement values recorded for each point in parameter space.)
        
        For simulation data, you can use the dimensions in whichever way you would like, you have 
        four of them at your disposal. The only limitation of the routines is that you cannot 
        plot line cuts / colorscale plots along the fourth dimension. 
        
        This will, of course, not fill any of the metadata in the datacube about the axis names
        or ranges, or "column" names (4th dimension, what is labelled on the y-axis of a linecut, 
        for example). For this, you will have to fill them in yourself:
        
        dc.axis[0].start = 1
        dc.axis[0].stop = 5
        dc.axis[0].name = "My name (units)"
        
        and 
        
        dc.column_name[0] = "Homodyne voltage (V)"
        dc.column_name[1] = "Leakage current (A)"
        
        etc.

        '''
        self.data = data.copy()
        shape = list(data.shape)
            
        if len(shape) > 4:
            raise Exception("Input array must have at most 4 dimensions")
        
        if len(shape) < 4:
            for i in range(len(shape)-4):
                shape.append(1)
            self.data = np.reshape(self.data,shape)
        
        for i in range(3):
            self.axis[i].size = shape[i]
            
        for i in range(shape[3]):
            self.column_name[i] = "Dim 4 index %d" % i
        
    #
    # Helper functions
    # 
    
    def print_info(self):
        for i in range(3):
            print("Axis %d: %s" % (i,self.axis[i].name))
            print("  Start %e" % (self.axis[i].start))
            print("   Stop %e" % (self.axis[i].stop))
            print("   Size %d" % (self.axis[i].size))
            print()
        print("Columns:")
        for k,v in self.column_name.items():
            print("   Col %d: %s" % (k,v))
        print()
        print("Current mtx settings:")
        print("   Cut axis: ", self.cut_axis)
        print("  Cut index: ", self.cut_index)
    
    def axis_val(self,ind,ax_num):
        axis = self.axis[ax_num]
        if axis.size == 1:
            return 0
        return axis.start + ind*(axis.stop-axis.start)/(axis.size-1)
    
    # 
    # Line cuts
    # 
    
    def interactive_linecut(self):
        axis_val = self.axis_val # historical...
        axis_w = widgets.IntSlider(max=2, description="Along axis:", value=0)
        index_w = []
        for ax in self.axis:
            index_w.append(widgets.IntSlider(max=ax.size-1, 
                                             description="%s (%d):" % 
                                             (ax.name,ax.size)))
        index_w[0].disabled = True
        
        column_w = widgets.Dropdown(options=list(zip(self.column_name.values(),self.column_name.keys())),
                                    description= "Column:")
        column_w.value = self.default_column
        
        autoscale_types = {1 : "both", 2: "x", 3: "y"}
        autoscale_options = [("Autoscale Both", 0), ("Full dataset range", 1), 
                            ("Keep X Zoom, Autoscale Y",2), ("Off (keep zoom)",3)]
        autoscale_w = widgets.Dropdown(options=autoscale_options, description="Scale:")
        
        axis_display = widgets.Label()
        index_display = [widgets.Label() for i in range(3)]
        
        left = widgets.VBox([axis_w,index_w[0],index_w[1],index_w[2],column_w,autoscale_w])
        right = widgets.VBox([axis_display, index_display[0], index_display[1], index_display[2]])
        
        for w in left.children:
            w.layout = widgets.Layout(width="600px")
            w.style = {'description_width': '100px'}
        
        ui = widgets.HBox([left,right])

        def autoscale_array(x):
            x1 = np.min(x)
            x2 = np.max(x)
            span = x2-x1
            return (x1-0.1*span, x2+0.1*span)
        
        def adjust_scale():
            if autoscale_w.value == 0:
                mpl_ax.set_xlim(autoscale_array(x))
                mpl_ax.set_ylim(autoscale_array(y))
            elif autoscale_w.value == 1:
                mpl_ax.set_xlim(autoscale_array(x))
                mpl_ax.set_ylim(autoscale_array(self.data[:,:,:,column_w.value]))
            elif autoscale_w.value == 2:
                x1,x2 = mpl_ax.get_xlim()
                i1 = get_x_ind(x1)
                i2 = get_x_ind(x2)
                mpl_ax.set_ylim(autoscale_array(y[i1:i2+1]))
        
        # Return an index of x based on a value (reverse lookup)
        def get_x_ind(xval):
            step = x[1]-x[0]
            ind = (xval-x[0])/step
            if ind < 0:
                ind = 0
            if ind >= len(x):
                ind = len(x)-1
            return int(round(ind))
    
        def update_axis(w):
            # If axis changes, we need to change the x-vector for our plot
            nonlocal x
            axis = self.axis[axis_w.value]
            x = np.linspace(axis.start, axis.stop, axis.size)
            mpl_ax.set_xlabel(axis.name)
            axis_display.value = axis.name
            # And we need to disable the correct sliders
            for i in range(3):
                if i == axis_w.value:
                    index_w[i].disabled = True
                else:
                    index_w[i].disabled = False
        axis_w.observe(update_axis)
        
        def update_column(w):
            plt.ylabel(self.column_name[column_w.value])        
        column_w.observe(update_column)
        
        def update_displays():
            fmt = "%s = %e"
            for i in range(3):
                index_display[i].value = fmt % (self.axis[i].name, axis_val(index_w[i].value,i))
            
        def update_plot(axis, index0, index1, index2, autoscale, column):
            nonlocal y
            update_axis([])
            update_displays()
            title = []
            # To do: intellegently pick the number of digits
            fmt = "%s = %.4e"
            if axis == 0:
                y = self.data[:,index1,index2,column_w.value]
                title = fmt % (self.axis[1].name, axis_val(index1,1))
                title += ", " + fmt % (self.axis[2].name, axis_val(index2,2))
            elif axis == 1:
                y = self.data[index0,:,index2,column_w.value]
                title = fmt % (self.axis[0].name, axis_val(index0,0))
                title += ", " + fmt % (self.axis[2].name, axis_val(index2,2))
            elif axis == 2:
                y = self.data[index0,index1,:,column_w.value]
                title = fmt % (self.axis[0].name, axis_val(index0,0))
                title += ", " + fmt % (self.axis[1].name, axis_val(index1,1))
            mpl_ax.set_title(title,fontsize=10) 
            adjust_scale()
            line.set_data(x,y)
        
        # This one will be updated when the widget changes
        x = np.linspace(0,1)
        y = x
        
        # Create an empty plot that we will update the line of later
        # This is how we do "live" updates with the notebook driver
        fig, mpl_ax = plt.subplots()
        line, = mpl_ax.plot(x,y)
                
        update_column([])
        update_axis([])
        
        out = widgets.interactive_output(update_plot, {'axis': axis_w, 
                                                       'index0': index_w[0],
                                                       'index1': index_w[1],
                                                       'index2': index_w[2],
                                                       'autoscale': autoscale_w,
                                                       'column': column_w
                                                      })
        display(ui, out)
    
    def plot_linecut(self, index = (0,0,0), along_axis = 0, col = 'default', ax=None):
        '''
        plot a linecut of the datacube. 
        
        index: specifies the 3 axis indices (one of which is not used!)
        
        along_axis: specifies which axis to take the line cut along
        
        col: specifies which column number. Value 'default' specifies that class
             defined default_column should be used.         
        '''
        
        if col == 'default':
            col = self.default_column
        axis_val = self.axis_val # historical, and to save typing...
        x = np.linspace(self.axis[along_axis].start, 
                        self.axis[along_axis].stop, 
                        self.axis[along_axis].size)
        
        fmt = "%s = %.4e"
        if along_axis == 0:
            y = self.data[:,index[1],index[2],col]
            title = fmt % (self.axis[1].name, axis_val(index[1],1))
            title += ", " + fmt % (self.axis[2].name, axis_val(index[2],2))
        elif along_axis == 1:
            y = self.data[index[0],:,index[2],col]
            title = fmt % (self.axis[0].name, axis_val(index0,0))
            title += ", " + fmt % (self.axis[2].name, axis_val(index2,2))
        elif along_axis == 2:
            y = self.data[index[0],index[1],:,col]
            title = fmt % (self.axis[0].name, axis_val(index[0],0))
            title += ", " + fmt % (self.axis[1].name, axis_val(index[1],1))
    
        if ax == None:
            fig, ax = plt.subplots()
        ax.plot(x,y)
        ax.set_xlabel(self.axis[along_axis].name)
        ax.set_ylabel(self.column_name[col])
        ax.set_title(title, fontsize=10)

    #
    # Color plots
    # 
    
    # Googled this one
    def truncate_colormap(self, minval=0.0, maxval=1.0, cmapIn='RdBu_r'):
        cmapIn = plt.get_cmap(cmapIn)
        new_cmap = colors.LinearSegmentedColormap.from_list(
            'trunc({n},{a:.2f},{b:.2f})'.format(n=cmapIn.name, a=minval, b=maxval),
            cmapIn(np.linspace(minval, maxval, 256)))
        return new_cmap
    
    def create_mtx_cut(self, cut_axis = 2, cut_index = 0, col = 'default'):
        if col == 'default':
            col = self.default_column
        n1 = (cut_axis + 1) % 3
        n2 = (cut_axis + 2) % 3
        # This seems a bit silly but it's the only way to get our data into a stlabutils mtx...
        xvals = np.linspace(self.axis[n1].start, self.axis[n1].stop, self.axis[n1].size)
        yvals = np.linspace(self.axis[n2].start, self.axis[n2].stop, self.axis[n2].size)
        if cut_axis == 2:
            df = pd.DataFrame(self.data[:,:,cut_index,col].T,columns=xvals, index=yvals)
        elif cut_axis == 0:
            df = pd.DataFrame(self.data[cut_index,:,:,col].T,columns=xvals, index=yvals)
        elif cut_axis == 1:
            df = pd.DataFrame(self.data[:,cut_index,:,col],columns=xvals, index=yvals)
        self.mtx = stlabutils.stlabmtx(df, xtitle=self.axis[n1].name, 
                                                  ytitle=self.axis[n2].name, 
                                                  ztitle=self.column_name[col])
        self.cut_axis = cut_axis
        self.cut_index = cut_index
        
    def plot_image(self,ax=None,cmap='RdBu_r'):
        if self.cut_axis == -1:
            self.create_mtx_cut()
        if ax == None:
            fig,ax = plt.subplots()
        im = ax.imshow(self.mtx.pmtx, extent=self.get_extent(),
                  norm=colors.PowerNorm(gamma=self.gamma),
                  aspect='auto',cmap=cmap, origin='lower')
        ax.set_xlabel(self.mtx.xtitle)
        ax.set_ylabel(self.mtx.ytitle)
        plt.colorbar(im).set_label(self.mtx.ztitle)

    def get_extent(self):
        # extent : scalars (left, right, bottom, top)
        # I think Mark coded getextents() assuming the user would not be using the 
        # origin_lower option...
        e = self.mtx.getextents()
        e = (e[0], e[1], e[3], e[2])
        return e
        
    def interactive_image(self):
        fig, ax = plt.subplots()
        
        img = plt.imshow(self.mtx.pmtx, extent=self.get_extent(), 
           norm=colors.PowerNorm(gamma=self.gamma),
           aspect='auto',cmap='RdBu_r', origin='lower')
        
        cbar = plt.colorbar()
        def update_plot_labels(w):
            plt.xlabel(self.mtx.xtitle)
            plt.ylabel(self.mtx.ytitle)
            cbar.set_label(self.mtx.ztitle)
            
        update_plot_labels([])
            
        def update_plot_colors(w):
            self.gamma = gamma.value
            img.set_cmap(self.truncate_colormap(cmap_min.value, cmap_max.value, colormap.value))
            vmin = self.mtx.pmtx.quantile(min_quantile.value).quantile(min_quantile.value)
            vmax = self.mtx.pmtx.quantile(max_quantile.value).quantile(max_quantile.value)
            img.set_norm(norm=colors.PowerNorm(gamma=gamma.value, vmin=vmin, vmax=vmax))
        
        gamma = widgets.FloatLogSlider(value = 1, base=10, min=-1, max = 1, 
                                         description = "gamma",
                                 step = 0.05, readout_format = ".2f")
        min_quantile = widgets.FloatSlider(value = 0.02, min=0, max=1, 
                                              description = "min %tile",
                                             step=0.001,readout_format = ".3f")
        max_quantile = widgets.FloatSlider(value = 0.98, min=0, max=1,
                                            description = "max %tile",
                                             step=0.001,readout_format = ".3f")
        cmap_min = widgets.FloatSlider(min=0, max=1, step=0.01,value=0,
                                        description = "cmap max")
        cmap_max = widgets.FloatSlider(min=0, max=1, step=0.01,value=1,
                                        description = "cmap min")
        
        ui_left = widgets.VBox([gamma, min_quantile, max_quantile, 
                          cmap_min, cmap_max])
        
        for w in ui_left.children:
            w.layout = widgets.Layout(width="400px")
            w.style = {'description_width': '100px'}
            w.observe(update_plot_colors)

        def update_image(w):
            img.set_data(self.mtx.pmtx)
            update_plot_colors([])
            update_plot_labels([])
            
        def update_cut(w):
            self.create_mtx_cut(cut_axis.value, cut_index.value, column.value)
            img.set_extent(self.get_extent())
            update_image([])
        
        def update_cut_axis(w):
            cut_index.max = self.data.shape[cut_axis.value]
            cut_index.value = 0 # probably a reasonable default when changing axis cut
            update_cut([])
        
        cut_axis = widgets.IntSlider(min=0, max = 2, value=2, description="Cut axis:")
        cut_axis.observe(update_cut_axis)
        
        cut_index = widgets.IntSlider(min=0, max = self.data.shape[2], description="Cut index:")
        cut_index.observe(update_cut)
        
        column = widgets.Dropdown(options=list(zip(self.column_name.values(),self.column_name.keys())),
                                    description= "Column:")
        column.value = self.default_column
        column.observe(update_cut)
        
        colormap = widgets.Dropdown(options=plt.colormaps(), description="Colormap:")
        colormap.value = "RdBu_r"
        colormap.observe(update_plot_colors)
        
        def update_cmap(w):
            colormap.value = plt.colormaps()[colormap_slider.value]
        
        colormap_slider = widgets.IntSlider(max=len(plt.colormaps()), 
                                            value=plt.colormaps().index(colormap.value),
                                           description = "Cmap slider:")
        colormap_slider.observe(update_cmap)
    
        ui_right = widgets.VBox([cut_axis, cut_index, column, colormap, colormap_slider])
        ui = widgets.HBox([ui_left, ui_right])
        display(ui)
        update_cut_axis([])
# -

# datfile = '/home/jovyan/steelelab/measurement_data/Triton/Stefanos/S0_2020_07_20_13.16.24_TWPA_power_sweeps/S0_2020_07_20_13.16.24_TWPA_power_sweeps.dat'
#
#
# dc = Datacube()
#
#
# dc.load_dat_meta(datfile)
#
# dc.plot_image()
